/* 
  Problem 4: Write a function that will use the previously written functions to get the following information. 
    You do not need to pass control back to the code that called it.

    Get information from
the Thanos boards
    Get all the lists for the Thanos board
    Get all cards for the Mind list simultaneously
*/

const { getBoardData } = require("./callback1");
const { getList } = require("./callback2");
const { getCard } = require("./callback3");

function getData(boards, lists, cards) {
  getBoardData("Thanos", boards, (err, board) => {
    if (err) console.log(err.message);
    console.log(board);
    getList(board[0].id, lists, (err, lists) => {
      if (err) console.log(err.message);
      console.log(lists);
      let data = lists.filter((item) => {
        return item.name === "Mind";
      });
      console.log(data);
      getCard(data[0].id, cards, (err, card) => {
        if (err) console.log(err.message);
        console.log(card);
      });
    });
  });
}

module.exports = { getData };
