/* 
  Problem 1: Write a function that will return a particular board's information based on the
     boardID that is passed from the given list of boards in boards.json and 
     then pass control back to the code that called it by using a callback function.
*/

function getBoardData(id, boards, cb) {
  let err
  if (typeof boards !== "object")
    err = new Error(`${boards} is not a valid json`);
  if (
    Object.keys(boards).length === 0 ||
    (Array.isArray(boards) && Object.keys(boards[0]).length === 0)
  )
    err = new Error(`boards is an empty object/array `);

  return setTimeout(() => {
    let arr = boards.filter((boards) => {
      return boards.name == id || boards.id == id;
    });
    if (arr.length === 0) {
      err = new Error(`No board found with id ${id}`);
    }
    cb(err, arr);
  }, 2000);
}

module.exports = { getBoardData };
