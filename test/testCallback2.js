const lists = require("../data/lists.json");
const { getList } = require("../callback2.js");

getList("mcu453ed", lists, (err, list) => {
    if (err) console.log(err.message);
    else {
        console.log(list);
    }
});

