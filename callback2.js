/* 
  Problem 2: Write a function that will return all lists that belong to a board based on the boardID that is passed to it 
    from the given data in lists.json. Then pass control back to the code that called it by using a callback function.
*/

function getList(id, lists, cb) {
  let err
  if (typeof lists !== "object")
    err = new Error(`${lists} is not a valid json`);
  if (
    Object.keys(lists).length === 0 ||
    (Array.isArray(lists) && Object.keys(lists[0]).length === 0)
  )
    err = new Error(`lists is an empty object/array `);

  return setTimeout(() => {
    if (lists[id]) cb(err, lists[id]);
    else {
      err = new Error(`no list found with given id : ${id}`)
      cb(err)
    }
  }, 2000);
}

module.exports = { getList };
