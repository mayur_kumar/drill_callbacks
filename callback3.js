/* 
    Problem 3: Write a function that will return all cards that belong to a 
    particular list based on the listID that is passed to it from the given data in cards.json. 
    Then pass control back to the code that called it by using a callback function.
*/

function getCard(id, cards, cb) {
    let err
    if (typeof cards !== "object")
        err = new Error(`${cards} is not a valid json`);
    if (
        Object.keys(cards).length === 0 ||
        (Array.isArray(cards) && Object.keys(cards[0]).length === 0)
    )
        err = new Error(`cards is an empty object/array `);

    return setTimeout(() => {
        if (cards[id]) cb(err, cards[id]);
        else {
            err = new Error(`no card found with id :${id}`);
            cb(err)
        }
    }, 2000);
}

module.exports = { getCard };
